data EssayS = Unassigned
            | Assigned
            | Writing
            | Reviewing
            | Done
data Grade' = A | B | C | D | E

Topic : Type
Topic = String

record StudentId where
  constructor MkStudentId
  value : Nat
record Essay where
  constructor MkEssay
  topic   : Topic
  student : StudentId
  state   : EssayS
  grade   : Grade'
  review  : String

illEssay : Essay
illEssay = MkEssay "Illegal" (MkStudentId 3) Writing C "Not reviewed"
