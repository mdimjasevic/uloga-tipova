data Grade' = A | B | C | D | E

data EssayS = Unassigned
            | Assigned
            | Writing
            | Reviewing
            | Done

data EssayCmd : Type -> EssayS -> EssayS -> Type where
  Assign   : EssayCmd () Unassigned Assigned
  Start    : EssayCmd () Assigned Writing
  Consult  : EssayCmd () state state
  Review   : EssayCmd () Writing Reviewing
  SendBack : EssayCmd () Reviewing Writing
  Grade    : Grade' -> EssayCmd Grade' Reviewing Done
  
  Pure     : ty -> EssayCmd ty state state
  (>>=)    : EssayCmd a state1 state2 ->
             (a -> EssayCmd b state2 state3) ->
             EssayCmd b state1 state3

essayProg1 : EssayCmd () Unassigned Assigned
essayProg1 = do
  Consult
  Assign

essayProg2 : EssayCmd () Writing Writing
essayProg2 = do
  Review
  SendBack

essayProg3 : EssayCmd Grade' Reviewing Done
essayProg3 = do
  SendBack
  Review
  Grade C
