import Data.Vect

minZip : Vect n a -> Vect (n + m) b -> Vect n (a, b)
minZip [] _ = []
minZip (x :: xs) (y :: ys) = (x, y) :: minZip xs ys


minZip2 : Vect n a -> Vect m b -> Vect (minimum n m) (a, b)
minZip2 [] _ = []
minZip2 (_ :: _) [] = []
minZip2 (x :: xs) (y :: ys) = (x, y) :: minZip2 xs ys

minimumZ : (n : Nat) -> minimum n Z = Z
minimumZ Z = Refl
minimumZ (S k) = Refl

minZip3 : Vect n a -> Vect m b -> Vect (minimum n m) (a, b)
minZip3 [] _ = []
minZip3 {n} _ [] = rewrite minimumZ n in []
minZip3 (x :: xs) (y :: ys) = (x, y) :: minZip3 xs ys
