data Grade' = A | B | C | D | E

Topic : Type
Topic = String

record StudentId where
  constructor MkStudentId
  value : Int

record TeacherId where
  constructor MkTeacherId
  value : Int

data Essay' = Unassigned' Topic
            | Assigned' Topic StudentId
            | Writing' Topic StudentId
            | Reviewing' Topic StudentId String
            | Done' Topic StudentId Grade'

total
fun : Essay' -> TeacherId -> String
fun (Unassigned' t) tId    = ?fun_rhs_1
fun (Assigned' t s) tId    = ?fun_rhs_2
fun (Writing' t s) tId     = ?fun_rhs_3
fun (Reviewing' t s r) tId = ?fun_rhs_4
fun (Done' t s g) tId      = ?fun_rhs_5
